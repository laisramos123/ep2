﻿# EP2 - OO 2019.2 (UnB - Gama)

 

## Descrição
	

	Esta é uma das versões do clássico SNAKE GAME que requer do seu jogador agilidade, atenção e habilidade .Tem por objetivos o desvio de obstáculos, o desvio do próprio corpo da cobrinha e claro, a captura de frutinhas. 
 
## 

 

  

## Intruções
	

	O jogo é dividido em três modos de Cobra.Para acessar esses três modos o jogador necessita escolher o modo que deseja no menu lateral, sendo que ,ao clicar no modo desejado, uma janela abre com o mesmo selecionado. Caso deseje alterar o modo, basta aprenas fechar a janela e selecionar outro modo. Ele está dividido da seguinte meneira:

* **Normal**  
* **Kitty**  
* **Star**  


	O objetivo principal é a captura de frutas!Mas cuidado, cada captura ocasiona em um efeito diferente! 

* **Simple Fruit:1 ponto ao Score e mais um elo à cobrinha**  
* **Bomb Fruit:Game Over**  
* **Big Fruit:2 pontos ao Score e mais dois elos à cobrinha**  
* **Decrease Fruit:O Score atual é reduzido à metade, porém mesmo assim a cobrinha ainda ganha um elo**  

**OBS.:Caso a cobra esbarre em uma das barreiras, é Game Over também!**  
 

## Dependências e bibliotecas utilizadas
	
  java.awt.Color;
  java.awt.Font;
  java.awt.Graphics;
  java.awt.event.KeyEvent;
  java.awt.event.KeyListener;
  java.util.Random;
  java.awt.event.ActionListener;
  java.awt.event.ActionEvent;
  javax.swing.ImageIcon;
  javax.swing.JFrame;
  javax.swing.JPanel;
  javax.swing.Timer;
  java.awt.Color;
  java.awt.Container;
  javax.swing.ImageIcon;
  javax.swing.JButton;
  javax.swing.JLabel;
  javax.swing.JMenu;
  javax.swing.JMenuBar;
  javax.swing.JMenuItem;
  javax.swing.JPanel;


## IDE utilizada : Eclipse SDK , version 2018-09 (4.9)
  



 