package snake2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Star extends Normal {

	/**
	 * 
	 */
	 

	public Star() {
		// TODO Auto-generated constructor stub
	}
	
	public void paint(Graphics g) {

		if(moves == 0) {
			snakeXlenght[2]=50;
			snakeXlenght[1]= 75;
			snakeXlenght[0]= 100;
			
			snakeYlenght[2]=100;
			snakeYlenght[1]=100;
			snakeYlenght[0]=100;
		}
		
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);
		
		titleImage = new ImageIcon("snakeTitle.png");
		titleImage.paintIcon(this, g, 25, 11);
		
		
		g.setColor(Color.white);
		g.drawRect(24, 74, 851, 577);
		
		
		g.setColor(Color.black);
		g.fillRect(25, 75, 850, 575);
		
		 
		
		g.setColor(Color.green);
		g.setFont(new Font("arial",Font.BOLD,14));
		g.drawString("Scores: "+score, 780, 30);
		
		g.setColor(Color.green);
		g.setFont(new Font("arial",Font.BOLD,14));
		g.drawString("Length: "+lengthofsnake, 780, 50);
		
			
		cabeca_direita = new ImageIcon("cabeca_direita.png");
		cabeca_direita.paintIcon(this, g, snakeXlenght[0], snakeYlenght[0]);
		
		for(int i = 0; i<lengthofsnake;i++) {
			if(i==0 && right) {
				cabeca_direita = new ImageIcon("cabeca_direita.png");
				cabeca_direita.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			if(i==0 && left) {
				cabeca_esquerda = new ImageIcon("cabeca_esquerda.png");
				cabeca_esquerda.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			if(i==0 && down) {
				cabeca_baixo = new ImageIcon("cabeca_baixo.png");
				cabeca_baixo.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			if(i==0 && up) {
				cabeca_cima = new ImageIcon("cabeca_cima.png");
				cabeca_cima.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			
			if(i!=0) {
				bola = new ImageIcon("bola.png");
				bola.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
		}
		
		metadeFruit=new ImageIcon("comida3.png");
		frutaImage = new ImageIcon("comida.png");
		bomba = new ImageIcon("bomba.png");
		especialFruit = new ImageIcon("comida2.png");
		obstaculo = new ImageIcon("obstaculo.png");
		if(enemyXpos[xpos] == snakeXlenght[0] && enemyYpos[ypos] == snakeYlenght[0]  ) {
			
			score=score+2;
			lengthofsnake++;
			xpos=random.nextInt(34);
			ypos=random.nextInt(23);
			trocaFruta=1;
		}else 
		if(bombXpos[xBombPos]==snakeXlenght[0] && bombYpos[yBombPos] == snakeYlenght[0]) {
			right = false;
			left = false;
			up = false;
			down = false;
			
			g.setColor(Color.green);
			g.setFont( new Font("arial", Font.BOLD,50));
			g.drawString("GAME OVER", 300, 300);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Scores:"+score   , 370, 390);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Space to RESTART", 350, 340);
			

		}else
			if(especialFruitXpos[especialFruitxpos] == snakeXlenght[0] && especialFruitYpos[especialFruitypos] == snakeYlenght[0]) {
			score=score+4;
			lengthofsnake=lengthofsnake+2;
			especialFruitxpos=random.nextInt(34);
			especialFruitypos=random.nextInt(23);
			trocaFruta=2;
			
		}else
			if( metaFruitXpos[metaxpos] == snakeXlenght[0] && metaFruitYpos[metaypos] == snakeYlenght[0]) {
			score=score/2;
			lengthofsnake=lengthofsnake+2;
			especialFruitxpos=random.nextInt(34);
			especialFruitypos=random.nextInt(23);
			trocaFruta=0;
			
		}
			else 
			if(obstaculoXpos[obstxpos] == snakeXlenght[0] && obstaculoYpos[obstypos] == snakeYlenght[0]) {
			right = false;
			left = false;
			up = false;
			down = false;
			
			g.setColor(Color.green);
			g.setFont( new Font("arial", Font.BOLD,50));
			g.drawString("GAME OVER", 300, 300);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Scores:"+score   , 370, 390);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Space to RESTART", 350, 340);
		}
		
		obstaculo.paintIcon(this, g, obstaculoXpos[obstxpos], obstaculoYpos[obstypos] );
		//especialFruit.paintIcon(this, g, especialFruitXpos[especialFruitxpos], especialFruitYpos[especialFruitypos] );
		bomba.paintIcon(this, g, bombXpos[xBombPos], bombYpos[yBombPos]);
		//frutaImage.paintIcon(this, g, enemyXpos[xpos], enemyYpos[ypos]);
		
		if(trocaFruta==0) {
			frutaImage.paintIcon(this, g, enemyXpos[xpos], enemyYpos[ypos]);
		}else if(trocaFruta==1) {
			especialFruit.paintIcon(this, g, especialFruitXpos[especialFruitxpos], especialFruitYpos[especialFruitypos] );
		}else if(trocaFruta==2) {
			metadeFruit.paintIcon(this, g, metaFruitXpos[metaxpos], metaFruitYpos[metaypos]);
		}
		
		
		for(int b = 1;b< lengthofsnake;b++) {
			if(snakeXlenght[b] == snakeXlenght[0] && snakeYlenght[b] == snakeYlenght[0]) {
				
				right = false;
				left = false;
				up = false;
				down = false;
				
				g.setColor(Color.green);
				g.setFont( new Font("arial", Font.BOLD,50));
				g.drawString("GAME OVER", 300, 300);
				
				g.setFont( new Font("arial", Font.BOLD,20));
				g.drawString("Scores:"+score   , 370, 390);
				
				g.setFont( new Font("arial", Font.BOLD,20));
				g.drawString("Space to RESTART", 370, 440);
				
				
				
				
			}
		}
		
		g.dispose();
		
	}
	
}
