package snake2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;


public class Normal extends JPanel implements KeyListener,ActionListener {
	 
	 
	protected int [] snakeXlenght = new int[750];
	protected int [] snakeYlenght = new int[750];
	
	protected boolean left = false;
	protected boolean right = false;
	protected boolean up=false;
	protected boolean down = false;
	
	protected ImageIcon  cabeca_direita;
	protected ImageIcon  cabeca_esquerda;
	protected ImageIcon  cabeca_baixo;
	protected ImageIcon  cabeca_cima;
	protected ImageIcon titleImage;
	protected ImageIcon frutaImage;
	
	protected Timer timer;
	protected int delay = 100;
	protected ImageIcon bola;
	protected ImageIcon bomba;
	protected ImageIcon especialFruit;
	protected ImageIcon obstaculo;
	protected ImageIcon obstaculo2;
	protected ImageIcon obstaculo3;
	protected ImageIcon obstaculo4;
	protected ImageIcon obstaculo5;
	protected ImageIcon obstaculo6;
	protected ImageIcon obstaculo7;
	protected ImageIcon metadeFruit;
	
	 
	
	protected int[] metaFruitXpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] metaFruitYpos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] obstaculo6Xpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculo6Ypos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	
	protected int[] obstaculo7Xpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculo7Ypos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	
	protected int[] obstaculo4Xpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculo4Ypos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] obstaculo5Xpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculo5Ypos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	
	protected int[] obstaculoXpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculoYpos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] obstaculo2Xpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculo2Ypos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] obstaculo3Xpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] obstaculo3Ypos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] especialFruitXpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] especialFruitYpos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] bombXpos =  {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] bombYpos =  {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	protected int[] enemyXpos= {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	protected int[] enemyYpos= {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	 
	
	protected Random random = new Random();
	protected Random random2 = new Random();
	
	 
	
	protected int xpos = random.nextInt(34);
	protected int ypos = random.nextInt(23);
	
	protected int xBombPos = random.nextInt(34);
	protected int yBombPos = random.nextInt(23);
	
	protected int especialFruitxpos = random.nextInt(34);
	protected int especialFruitypos = random.nextInt(23);
	
	protected int obstxpos = random.nextInt(34);
	protected int obstypos = random.nextInt(23);
	
	protected int obst2xpos = random.nextInt(34);
	protected int obst2ypos = random.nextInt(23);
	
	protected int obst3xpos = random.nextInt(34);
	protected int obst3ypos = random.nextInt(23);
	
	protected int obst4xpos = random.nextInt(34);
	protected int obst4ypos = random.nextInt(23);
	
	protected int obst5xpos = random.nextInt(34);
	protected int obst5ypos = random.nextInt(23);
	
	protected int obst6xpos = random.nextInt(34);
	protected int obst6ypos = random.nextInt(23);
	
	protected int obst7xpos = random.nextInt(34);
	protected int obst7ypos = random.nextInt(23);
	
	protected int metaxpos = random.nextInt(34);
	protected int metaypos = random.nextInt(23);
	
	protected int sequenciaFruit = 1;
	
	protected int score = 0;
	
	protected int lengthofsnake = 3;
	protected int moves = 0;
	protected int trocaFruta;
	protected int trocaFruta2=0;
	 
	
	
	
	public Normal() {
		// TODO Auto-generated constructor stub
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay,this);
		timer.start();
	}
	  
	public void paint(Graphics g) {
		
		if(moves == 0) {
			snakeXlenght[2]=50;
			snakeXlenght[1]= 75;
			snakeXlenght[0]= 100;
			
			snakeYlenght[2]=100;
			snakeYlenght[1]=100;
			snakeYlenght[0]=100;
		}
		
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);
		
		titleImage = new ImageIcon("snakeTitle.png");
		titleImage.paintIcon(this, g, 25, 11);
		
		
		g.setColor(Color.white);
		g.drawRect(24, 74, 851, 577);
		
		
		g.setColor(Color.black);
		g.fillRect(25, 75, 850, 575);
		
		 
		
		g.setColor(Color.green);
		g.setFont(new Font("arial",Font.BOLD,14));
		g.drawString("Scores: "+score, 780, 30);
		
		g.setColor(Color.green);
		g.setFont(new Font("arial",Font.BOLD,14));
		g.drawString("Length: "+lengthofsnake, 780, 50);
		
			
		cabeca_direita = new ImageIcon("cabeca_direita.png");
		cabeca_direita.paintIcon(this, g, snakeXlenght[0], snakeYlenght[0]);
		
		for(int i = 0; i<lengthofsnake;i++) {
			if(i==0 && right) {
				cabeca_direita = new ImageIcon("cabeca_direita.png");
				cabeca_direita.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			if(i==0 && left) {
				cabeca_esquerda = new ImageIcon("cabeca_esquerda.png");
				cabeca_esquerda.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			if(i==0 && down) {
				cabeca_baixo = new ImageIcon("cabeca_baixo.png");
				cabeca_baixo.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			if(i==0 && up) {
				cabeca_cima = new ImageIcon("cabeca_cima.png");
				cabeca_cima.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
			
			if(i!=0) {
				bola = new ImageIcon("bola.png");
				bola.paintIcon(this, g, snakeXlenght[i], snakeYlenght[i]);
			}
		}
		 
		
		metadeFruit=new ImageIcon("comida3.png");
		frutaImage = new ImageIcon("comida.png");
		bomba = new ImageIcon("bomba.png");
		especialFruit = new ImageIcon("comida2.png");
		obstaculo = new ImageIcon("obstaculo.png");
		obstaculo2 = new ImageIcon("obstaculo.png");
		obstaculo3 = new ImageIcon("obstaculo.png");
		obstaculo4 = new ImageIcon("obstaculo.png");
		obstaculo5 = new ImageIcon("obstaculo.png");
		obstaculo6 = new ImageIcon("obstaculo.png");
		obstaculo7 = new ImageIcon("obstaculo.png");
		if(enemyXpos[xpos] == snakeXlenght[0] && enemyYpos[ypos] == snakeYlenght[0]  ) {
			
			score++;
			lengthofsnake++;
			xpos=random.nextInt(34);
			ypos=random.nextInt(23);
			 
			sequenciaFruit=random2.nextInt(4);
		}else 
		if(bombXpos[xBombPos]==snakeXlenght[0] && bombYpos[yBombPos] == snakeYlenght[0]) {
			right = false;
			left = false;
			up = false;
			down = false;
			
			g.setColor(Color.green);
			g.setFont( new Font("arial", Font.BOLD,50));
			g.drawString("GAME OVER", 300, 300);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Scores:"+score   , 370, 390);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Space to RESTART", 350, 340);
			
			sequenciaFruit=random2.nextInt(4);
			 

		}else
			if(especialFruitXpos[especialFruitxpos] == snakeXlenght[0] && especialFruitYpos[especialFruitypos] == snakeYlenght[0]) {
			score=score+2;
			lengthofsnake=lengthofsnake+2;
			especialFruitxpos=random.nextInt(34);
			especialFruitypos=random.nextInt(23);
			sequenciaFruit=random2.nextInt(4);
			 
			
		}else
			if( metaFruitXpos[metaxpos] == snakeXlenght[0] && metaFruitYpos[metaypos] == snakeYlenght[0]) {
			score=score/2;
			lengthofsnake=lengthofsnake+2;
			metaxpos=random.nextInt(34);
			metaypos=random.nextInt(23);
			sequenciaFruit=random2.nextInt(4);
			 
			
		}
			else 
			if(obstaculoXpos[obstxpos] == snakeXlenght[0] && obstaculoYpos[obstypos] == snakeYlenght[0]) {
			right = false;
			left = false;
			up = false;
			down = false;
			
			g.setColor(Color.green);
			g.setFont( new Font("arial", Font.BOLD,50));
			g.drawString("GAME OVER", 300, 300);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Scores:"+score   , 370, 390);
			
			g.setFont( new Font("arial", Font.BOLD,20));
			g.drawString("Space to RESTART", 350, 340);
		}
			else 
				if(obstaculo2Xpos[obst2xpos] == snakeXlenght[0] && obstaculo2Ypos[obst2ypos] == snakeYlenght[0]) {
				right = false;
				left = false;
				up = false;
				down = false;
				
				g.setColor(Color.green);
				g.setFont( new Font("arial", Font.BOLD,50));
				g.drawString("GAME OVER", 300, 300);
				
				g.setFont( new Font("arial", Font.BOLD,20));
				g.drawString("Scores:"+score   , 370, 390);
				
				g.setFont( new Font("arial", Font.BOLD,20));
				g.drawString("Space to RESTART", 350, 340);
			}
				else 
					if(obstaculo3Xpos[obst3xpos] == snakeXlenght[0] && obstaculo3Ypos[obst3ypos] == snakeYlenght[0]) {
					right = false;
					left = false;
					up = false;
					down = false;
					
					g.setColor(Color.green);
					g.setFont( new Font("arial", Font.BOLD,50));
					g.drawString("GAME OVER", 300, 300);
					
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Scores:"+score   , 370, 390);
					
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Space to RESTART", 350, 340);
				}
				else 
					if(obstaculo4Xpos[obst4xpos] == snakeXlenght[0] && obstaculo4Ypos[obst4ypos] == snakeYlenght[0]) {
					right = false;
					left = false;
					up = false;
					down = false;
						
					g.setColor(Color.green);
					g.setFont( new Font("arial", Font.BOLD,50));
					g.drawString("GAME OVER", 300, 300);
						
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Scores:"+score   , 370, 390);
						
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Space to RESTART", 350, 340);
				}
					else 
					if(obstaculo5Xpos[obst5xpos] == snakeXlenght[0] && obstaculo5Ypos[obst5ypos] == snakeYlenght[0]) {
					right = false;
					left = false;
					up = false;
					down = false;
							
					g.setColor(Color.green);
					g.setFont( new Font("arial", Font.BOLD,50));
					g.drawString("GAME OVER", 300, 300);
							
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Scores:"+score   , 370, 390);
							
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Space to RESTART", 350, 340);
				}
					else 
					if(obstaculo7Xpos[obst7xpos] == snakeXlenght[0] && obstaculo7Ypos[obst7ypos] == snakeYlenght[0]) {
					right = false;
					left = false;
					up = false;
					down = false;
								
					g.setColor(Color.green);
					g.setFont( new Font("arial", Font.BOLD,50));
					g.drawString("GAME OVER", 300, 300);
								
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Scores:"+score   , 370, 390);
								
					g.setFont( new Font("arial", Font.BOLD,20));
					g.drawString("Space to RESTART", 350, 340);
				}
					else 
						if(obstaculo6Xpos[obst6xpos] == snakeXlenght[0] && obstaculo6Ypos[obst6ypos] == snakeYlenght[0]) {
						right = false;
						left = false;
						up = false;
						down = false;
									
						g.setColor(Color.green);
						g.setFont( new Font("arial", Font.BOLD,50));
						g.drawString("GAME OVER", 300, 300);
									
						g.setFont( new Font("arial", Font.BOLD,20));
						g.drawString("Scores:"+score   , 370, 390);
									
						g.setFont( new Font("arial", Font.BOLD,20));
						g.drawString("Space to RESTART", 350, 340);
					}
		obstaculo7.paintIcon(this, g, obstaculo7Xpos[obst7xpos], obstaculo7Ypos[obst7ypos] );
		obstaculo6.paintIcon(this, g, obstaculo6Xpos[obst6xpos], obstaculo6Ypos[obst6ypos] );
		obstaculo5.paintIcon(this, g, obstaculo5Xpos[obst5xpos], obstaculo5Ypos[obst5ypos] );
		obstaculo4.paintIcon(this, g, obstaculo4Xpos[obst4xpos], obstaculo4Ypos[obst4ypos] );
		obstaculo3.paintIcon(this, g, obstaculo3Xpos[obst3xpos], obstaculo3Ypos[obst3ypos] );
		obstaculo2.paintIcon(this, g, obstaculo2Xpos[obst2xpos], obstaculo2Ypos[obst2ypos] );
		obstaculo.paintIcon(this, g, obstaculoXpos[obstxpos], obstaculoYpos[obstypos] );
		 
		 
		if(sequenciaFruit==1) {
			frutaImage.paintIcon(this, g, enemyXpos[xpos], enemyYpos[ypos]);
			 
		}else if(sequenciaFruit==2) {
			especialFruit.paintIcon(this, g, especialFruitXpos[especialFruitxpos], especialFruitYpos[especialFruitypos] );
			 
		}else if(sequenciaFruit==3  ) {
			metadeFruit.paintIcon(this, g, metaFruitXpos[metaxpos], metaFruitYpos[metaypos]);
			 
		}else if(sequenciaFruit==0) {
			bomba.paintIcon(this, g, bombXpos[xBombPos], bombYpos[yBombPos]);
			 
		}
		
		
		for(int b = 1;b< lengthofsnake;b++) {
			if(snakeXlenght[b] == snakeXlenght[0] && snakeYlenght[b] == snakeYlenght[0]) {
				
				right = false;
				left = false;
				up = false;
				down = false;
				
				g.setColor(Color.green);
				g.setFont( new Font("arial", Font.BOLD,50));
				g.drawString("GAME OVER", 300, 300);
				
				g.setFont( new Font("arial", Font.BOLD,20));
				g.drawString("Scores:"+score   , 370, 390);
				
				g.setFont( new Font("arial", Font.BOLD,20));
				g.drawString("Space to RESTART", 370, 440);
				
				
				
				
			}
		}
		
		g.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		timer.start();
		if(right) {
			for(int i=lengthofsnake-1;i >= 0;i--){
				snakeYlenght[i+1]=snakeYlenght[i];
			}
			for(int i = lengthofsnake;i>=0;i--){
				if(i==0) {
					snakeXlenght[i]=snakeXlenght[i]+25;
				}else {
					snakeXlenght[i]=snakeXlenght[i-1];
				}
				if(snakeXlenght[i]>850){
					snakeXlenght[i]=25;
				}
			}
			repaint();
		}
		if(left) {
			for(int i=lengthofsnake-1;i >= 0;i--){
				snakeYlenght[i+1]=snakeYlenght[i];
			}
			for(int i = lengthofsnake;i>=0;i--){
				if(i==0) {
					snakeXlenght[i]=snakeXlenght[i]-25;
				}else {
					snakeXlenght[i]=snakeXlenght[i-1];
				}
				if(snakeXlenght[i]<25){
					snakeXlenght[i]=850;
				}
			}
			repaint();
			
		}
		if(up) {
			for(int i=lengthofsnake-1;i >= 0;i--){
				snakeXlenght[i+1]=snakeXlenght[i];
			}
			for(int i = lengthofsnake;i>=0;i--){
				if(i==0) {
					snakeYlenght[i]=snakeYlenght[i]-25;
				}else {
					snakeYlenght[i]=snakeYlenght[i-1];
				}
				if(snakeYlenght[i]<75){
					snakeYlenght[i]=625;
				}
			}
			repaint();
			
		}
		if(down) {
			for(int i=lengthofsnake-1;i >= 0;i--){
				snakeXlenght[i+1]=snakeXlenght[i];
			}
			for(int i = lengthofsnake;i>=0;i--){
				if(i==0) {
					snakeYlenght[i]=snakeYlenght[i]+25;
				}else {
					snakeYlenght[i]=snakeYlenght[i-1];
				}
				if(snakeYlenght[i]>625){
					snakeYlenght[i]=75;
				}
			}
			repaint();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode()==KeyEvent.VK_SPACE) {
			
			moves = 0;
			score = 0;
			lengthofsnake = 3;
			repaint();
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			
			moves++;
			right=true;
			if(!left) {
				right= true;
			}
			else {
				right=false;
				left= true;
			}
			up=false;
			down=false;
		} 
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			
			moves++;
			left=true;
			if(!right) {
				left= true;
			}
			else {
				left=false;
				right= true;
			}
			up=false;
			down=false;
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			
			moves++;
			up=true;
			if(!down) {
				up= true;
			}
			else {
				up=false;
				down= true;
			}
			left=false;
			right=false;
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			
			moves++;
			down=true;
			if(!up) {
				down= true;
			}
			else {
				down=false;
				up= true;
			}
			left=false;
			right=false;
		}
		
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
